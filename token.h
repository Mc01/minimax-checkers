#ifndef TOKEN_H
#define TOKEN_H

namespace Artificial {
    namespace Actors {
        class Attack;
        class Connection;
        class Token;
        class State;
    }
}

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <algorithm>
#include <iostream>
#include "game.h"

typedef std::shared_ptr<Artificial::Game> GamePointer;
typedef std::shared_ptr<Artificial::Actors::Connection> ConnectionPointer;

namespace Artificial {
    namespace Actors {
        struct Attack
        {
        public:
            Attack(QPointF);
            Attack(QPointF, QPointF);
            QPointF target;
            QPointF destination;
        };

        class Connection
        {
        public:
            Connection(unsigned int);
            Connection(unsigned int, std::vector<Attack>);
            void eatConnection(ConnectionPointer);
            void addAttack(Attack*);
            void crownOwner(unsigned int);
            unsigned int getOwner() const;
            std::vector<Attack> getAttacks() const;
        private:
            unsigned int owner;
            std::vector<Attack> attacks;
        };

        class Token : public QGraphicsPixmapItem
        {
        public:
            Token(GamePointer, unsigned int, unsigned int, QPixmap&);
            void mousePressEvent(QGraphicsSceneMouseEvent*);
            void mouseMoveEvent(QGraphicsSceneMouseEvent*);
            void mouseReleaseEvent(QGraphicsSceneMouseEvent*);
            unsigned int getId() const;
            unsigned int getDx() const;
            unsigned int getDy() const;
            void setDXDY(unsigned int, unsigned int);
            void crown(unsigned int);
            void destroy();
        private:
            //@CONST
            static const unsigned int MATCH_DISTANCE = 20;
            //@VAR
            GamePointer game;
            unsigned int owner_id;
            unsigned int id;
            unsigned int dx;
            unsigned int dy;
            QPointF start_position;
            QPointF drag_position;
            ConnectionPointer availableMovment;
            bool bValid = false;
            //@METHOD
            bool isValid();
            unsigned int getDistance(QPointF, QPointF);
        };

        class State
        {
        public:
            State(unsigned int, std::vector<std::vector<unsigned int>>);
            unsigned int getTurn() const;
            std::vector<std::vector<unsigned int> > getBoard() const;
        private:
            unsigned int turn;
            std::vector<std::vector<unsigned int>> board;
        };
    }
}

#endif // TOKEN_H
