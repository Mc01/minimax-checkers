#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    game = GamePointer(new Artificial::Game(ui->graphics, ui->menuCurrent));
    syncPlayers();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_red_is_hm_triggered()
{
    if (ui->red_is_ai->isChecked()) {
        game.get()->RED_IS_AI = false;
        ui->red_is_ai->setChecked(false);
    }
    ui->red_is_hm->setChecked(true);
}

void MainWindow::on_red_is_ai_triggered()
{
    if (ui->red_is_hm->isChecked()) {
        game.get()->RED_IS_AI = true;
        ui->red_is_hm->setChecked(false);
        if (game.get()->getCurrentTurn() == game.get()->PLAYER_RED_ID)
            game.get()->playAI();
    }
    ui->red_is_ai->setChecked(true);
}

void MainWindow::on_blue_is_hm_triggered()
{
    if (ui->blue_is_ai->isChecked()) {
        game.get()->BLUE_IS_AI = false;
        ui->blue_is_ai->setChecked(false);
    }
    ui->blue_is_hm->setChecked(true);
}

void MainWindow::on_blue_is_ai_triggered()
{
    if (ui->blue_is_hm->isChecked()) {
        game.get()->BLUE_IS_AI = true;
        ui->blue_is_hm->setChecked(false);
        if (game.get()->getCurrentTurn() == game.get()->PLAYER_BLUE_ID)
            game.get()->playAI();
    }
    ui->blue_is_ai->setChecked(true);
}

void MainWindow::on_new_game_triggered()
{
    game.get()->reset();
}

void MainWindow::syncPlayers()
{
    if (game.get()->BLUE_IS_AI) {
        ui->blue_is_ai->setChecked(true);
        ui->blue_is_hm->setChecked(false);
    }
    else {
        ui->blue_is_ai->setChecked(false);
        ui->blue_is_hm->setChecked(true);
    }

    if (game.get()->RED_IS_AI) {
        ui->red_is_ai->setChecked(true);
        ui->red_is_hm->setChecked(false);
    }
    else {
        ui->red_is_ai->setChecked(false);
        ui->red_is_hm->setChecked(true);
    }
}

void MainWindow::on_ai_is_4_triggered()
{
    game.get()->MINIMAX_DEPTH = 4;
    ui->ai_is_4->setChecked(true);
    ui->ai_is_6->setChecked(false);
    ui->ai_is_8->setChecked(false);
}

void MainWindow::on_ai_is_6_triggered()
{
    game.get()->MINIMAX_DEPTH = 6;
    ui->ai_is_4->setChecked(false);
    ui->ai_is_6->setChecked(true);
    ui->ai_is_8->setChecked(false);
}

void MainWindow::on_ai_is_8_triggered()
{
    game.get()->MINIMAX_DEPTH = 8;
    ui->ai_is_4->setChecked(false);
    ui->ai_is_6->setChecked(false);
    ui->ai_is_8->setChecked(true);
}
