#include "game.h"

//@PARAM
unsigned int Artificial::Replica::MINIMAX_DEPTH = 6;
//@CONST
const QString Artificial::Game::TURN_RED = "Current Turn: RED";
const QString Artificial::Game::TURN_BLUE = "Current Turn: BLUE";
//@PARAM
bool Artificial::Game::RED_IS_AI = false;
bool Artificial::Game::BLUE_IS_AI = true;
MenuPointer Artificial::Game::menu;

Artificial::Replica::Replica()
{
    std::cout << "Replica starting." << std::endl;
}

Artificial::Replica::Replica(StatePointer state)
{
    this->current_player_turn = state.get()->getTurn();
    this->board = state.get()->getBoard();
}

int Artificial::Replica::miniMax(int depth, bool is_max)
{
    int replicated_depth = 0;
    bool replicated_max = true;
    if (depth == 0 && sync_status == Artificial::Enums::SIMPLE) {
        return this->getEvaluation();
    }
    else {
        std::vector<ConnectionPointer> possibleVector;
        std::vector<ReplicaPointer> replicaVector;
        std::vector<int> solutions;
        //@SIMPLE
        if (sync_status == Artificial::Enums::SIMPLE) {
            possibleVector = gather();
            replicaVector = replicate(possibleVector);
            replicated_depth = depth - 1;
            replicated_max = is_max ? false : true;
        }
        //@COMPLEX
        else if (sync_status == Artificial::Enums::COMPLEX) {
            replicaVector = cloneVector;
            replicated_depth = depth;
            replicated_max = is_max ? true : false;
        }
        //@LIMIT REACHED
        if (replicaVector.size() == 0) return this->getEvaluation();
        if (is_max) {
            //@MINIMAX THAN BEST
            for (ReplicaPointer replicant : replicaVector) {
                solutions.push_back(replicant.get()->miniMax(replicated_depth, replicated_max));
            }
            return solutions.at(getSolutionIndex(solutions, true));
        }
        else {
            //@WORST THAN MINIMAX
            for (ReplicaPointer replicant : replicaVector) {
                solutions.push_back(replicant.get()->getEvaluation());
            }
            int index = getSolutionIndex(solutions, false);
            return ((ReplicaPointer) replicaVector.at(index)).get()->miniMax(replicated_depth, replicated_max);
        }
    }
    return -1;
}

ConnectionPointer Artificial::Replica::search(int depth)
{
    std::vector<ConnectionPointer> possibleVector = gather();
    std::cout << "After gather." << std::endl;
    if (possibleVector.size() == 0) {
        unsigned int error_id = ERROR_ID;
        return std::make_shared<Artificial::Actors::Connection>(error_id);
    }
    std::vector<ReplicaPointer> replicaVector = replicate(possibleVector);
    std::cout << "After replication." << std::endl;
    std::vector<int> solutions;
    for (ReplicaPointer replicant : replicaVector) {
        solutions.push_back(replicant.get()->miniMax(depth, true));
    }
    std::cout << "After minimax." << std::endl;
    return possibleVector.at(getSolutionIndex(solutions, true));
}

std::vector<ConnectionPointer> Artificial::Replica::gather()
{
    std::vector<ConnectionPointer> possibleVector;
    bool forceAlignedWithTurn = false;
    if (forceVector.size() != 0) {
        //@FORCE CLEANUP
        bool is_blue = (current_player_turn == PLAYER_BLUE_ID) && isBlue(((ConnectionPointer) forceVector.at(0)).get()->getOwner());
        bool is_red = (current_player_turn == PLAYER_RED_ID) && !isBlue(((ConnectionPointer) forceVector.at(0)).get()->getOwner());
        if (is_blue || is_red) forceAlignedWithTurn = true;
        else forceVector.clear();
    }
    if (forceAlignedWithTurn) {
        possibleVector = forceVector;
    }
    else {
        for (unsigned int i = 0; i < BOARD_HEIGHT; i++) {
            for (unsigned int j = 0; j < BOARD_WIDTH; j++) {
                unsigned int id = board[i][j];
                bool isBrokenBlue = current_player_turn == PLAYER_BLUE_ID && !isBlue(id);
                bool isBrokenRed = current_player_turn == PLAYER_RED_ID && isBlue(id);
                if (id == 0 || isBrokenBlue || isBrokenRed) continue;
                ConnectionPointer connection = getAvailableMovement(j, i);
                if (connection.get()->getAttacks().size() != 0) {
                    possibleVector.push_back(connection);
                }
            }
        }
    }
    return possibleVector;
}

std::vector<ReplicaPointer> Artificial::Replica::replicate(std::vector<ConnectionPointer> possibleVector)
{
    std::vector<ReplicaPointer> clones;
    StatePointer current_state = StatePointer(new Artificial::Actors::State(current_player_turn, board));
    for (unsigned int i = 0, size = possibleVector.size(); i < size; i++) {
      ReplicaPointer replica = ReplicaPointer(new Artificial::Replica(current_state));
      if (replica.get()->synchronize(possibleVector.at(i))) {
          clones.push_back(replica);
      }
    }
    return clones;
}

bool Artificial::Replica::synchronize(ConnectionPointer connection)
{
    QPointF position = getPosition(connection.get()->getOwner());
    if (connection.get()->getAttacks().size() == 1) {
        unsigned int request_turn = current_player_turn;
        //@REQUEST
        if (requestMovement(position.x(), position.y(), connection.get()->getAttacks().at(0))) {
            sync_status = Artificial::Enums::SIMPLE;
            //@CHAIN
            if (current_player_turn == request_turn) {
                //@INDEX
                int index = getConnectionIndex(connection.get()->getOwner(), forceVector);
                if (index == -1) return false;
                if (((ConnectionPointer) forceVector.at(index)).get()->getAttacks().size() == 1){
                    if (!synchronize(forceVector.at(index))) return false;
                }
                //@COMPLEX
                else {
                    sync_status = Artificial::Enums::COMPLEX;
                    if (!isolate(forceVector.at(index))) return false;
                }
            }
            return true;
        }
        return false;
    }
    else {
        //@COMPLEX
        sync_status = Artificial::Enums::COMPLEX;
        return isolate(connection) ? true : false;
    }
}

bool Artificial::Replica::isolate(ConnectionPointer connection) {
    //@DECLARE
    StatePointer current_state = StatePointer(new Artificial::Actors::State(current_player_turn, board));
    cloneVector.clear();
    for (unsigned int i = 0, size = connection.get()->getAttacks().size(); i < size; i++) {
        //@CLONNING
        ReplicaPointer clone = ReplicaPointer(new Artificial::Replica(current_state));
        std::vector<Artificial::Actors::Attack> clone_attack;
        clone_attack.push_back(connection.get()->getAttacks().at(i));
        ConnectionPointer clone_connection = ConnectionPointer(new Artificial::Actors::Connection(connection.get()->getOwner(), clone_attack));
        if (!clone.get()->synchronize(clone_connection)) return false;
        cloneVector.push_back(clone);
    }
    return true;
}

int Artificial::Replica::getEvaluation()
{
    int value = 0;
    bool is_red_turn = (current_player_turn == PLAYER_RED_ID);
    for (unsigned int i = 0; i < BOARD_HEIGHT; i++) {
        for (unsigned int j = 0; j < BOARD_WIDTH; j++) {
            if (board[i][j] != 0) {
                int id = board[i][j];
                bool is_blue = isBlue(id);
                if (isThrone(is_blue, id)) {
                    value += (is_red_turn && !is_blue) ? -20 : 20;
                }
                else {
                    //@TOP
                    if (i == 0)
                        value += (is_red_turn && !is_blue) ? -10 : 7;
                    //@BOT
                    else if (i == BOARD_HEIGHT)
                        value += !(is_red_turn && !is_blue) ? 10 : -7;
                    //@MID
                    if (i >= 4)
                        value += (is_red_turn && !is_blue) ? -5 : (3 + i);
                    else
                        value += !(is_red_turn && !is_blue) ? 5 : -(10 - i);
                }
            }
        }
    }
    return value;
}

ConnectionPointer Artificial::Replica::getAvailableMovement(unsigned int dx, unsigned int dy)
{
    //@DECLARE
    unsigned int id = board[dy][dx];
    ConnectionPointer availableConnection = ConnectionPointer(new Artificial::Actors::Connection(id));
    //@FORCED
    if (forceVector.size() != 0) {
        int index = getConnectionIndex(id, forceVector);
        if (index != -1) {
            availableConnection = forceVector.at(index);
            std::cout << "Forced this." << std::endl;
        }
        else {
            std::cout << "Forced other." << std::endl;
        }
    }
    else {
        availableConnection = getConnectionK(false, dx, dy);
    }
    return availableConnection;
}

bool Artificial::Replica::requestMovement(unsigned int dx, unsigned int dy, Artificial::Actors::Attack attack)
{
    if (board[attack.destination.y()][attack.destination.x()] == 0 && board[dy][dx] != 0) {
        if (forceVector.size() != 0) {
            makeMovement(dx, dy, attack);
            //@DESTROY
            unsigned int target_id = board[attack.target.y()][attack.target.x()];
            if (target_id != 0) {
                destroyTarget(attack, target_id);
            }
            //@CHAIN
            std::vector<Artificial::Actors::Attack> attacks = getConnectionK(true, attack.destination.x(), attack.destination.y()).get()->getAttacks();
            if (attacks.size() != 0) {
                unsigned int id = board[attack.destination.y()][attack.destination.x()];
                forceVector.clear();
                forceVector.push_back(ConnectionPointer(new Artificial::Actors::Connection(id, attacks)));
                //printForce();
                return true;
            }
        }
        else {
            makeMovement(dx, dy, attack);
        }
        //@FUTURE SIGHT
        renewForce(flipTurn(current_player_turn));
        endTurn();
        return true;
    }
    else return false;
}

bool Artificial::Replica::renewForce(unsigned int player_turn)
{
    forceVector.clear();
    bool turn_blue = (player_turn == PLAYER_BLUE_ID);
    for (unsigned int i = 0; i < BOARD_HEIGHT; i++) {
        for (unsigned int j = 0; j < BOARD_WIDTH; j++) {
            //@EMPTY
            unsigned int id = board[i][j];
            if (id == 0) continue;
            //@TURN
            bool is_blue = isBlue(id);
            if ((!turn_blue && is_blue) || (turn_blue && !is_blue)) continue;
            //@CHECK
            std::vector<Artificial::Actors::Attack> attacks = getConnectionK(true, j, i).get()->getAttacks();
            if (attacks.size() != 0) forceVector.push_back(ConnectionPointer(new Artificial::Actors::Connection(id, attacks)));
        }
    }
    //printForce();
    return (forceVector.size() != 0) ? true : false;
}

ConnectionPointer Artificial::Replica::getConnectionK(bool is_attack, unsigned int dx, unsigned int dy)
{
    unsigned int id = board[dy][dx];
    ConnectionPointer availableConnection;
    bool is_blue = isBlue(id);
    bool is_throne = isThrone(is_blue, id);
    if (is_throne) {
        availableConnection = ConnectionPointer(new Artificial::Actors::Connection(id));
        availableConnection.get()->eatConnection(getQueenK(true, true, is_attack, dx, dy));
        availableConnection.get()->eatConnection(getQueenK(true, false, is_attack, dx, dy));
        availableConnection.get()->eatConnection(getQueenK(false, true, is_attack, dx, dy));
        availableConnection.get()->eatConnection(getQueenK(false, false, is_attack, dx, dy));
    }
    else {
      availableConnection = getPathK(is_blue, is_attack, dx, dy);
    }
    return availableConnection;
}

ConnectionPointer Artificial::Replica::getQueenK(bool go_up, bool go_left, bool is_attack, unsigned int dx, unsigned int dy)
{
    unsigned int id = board[dy][dx];
    ConnectionPointer availableConnection = ConnectionPointer(new Artificial::Actors::Connection(id));
    AttackPointer attack;
    unsigned int ddx = dx, ddy = dy;
    bool is_valid;
    //@PATH
    do {
        is_valid = false;
        attack = getAttackK(go_up, go_left, false, ddx, ddy);
        if (validateAttack(id, attack.get())) {
            availableConnection.get()->addAttack(attack.get());
            ddx = attack.get()->destination.x();
            ddy = attack.get()->destination.y();
            is_valid = true;
        }
    }
    while (is_valid);
    //@ATTACK
    if (is_attack) {
        availableConnection = ConnectionPointer(new Artificial::Actors::Connection(id));
        attack = getAttackK(go_up, go_left, is_attack, ddx, ddy);
        if (validateAttack(id, attack.get())) {
            availableConnection.get()->addAttack(attack.get());
        }
    }
    return availableConnection;
}

ConnectionPointer Artificial::Replica::getPathK(bool is_blue, bool is_attack, unsigned int dx, unsigned int dy) {
    unsigned int id = board[dy][dx];
    ConnectionPointer availableConnection = ConnectionPointer(new Artificial::Actors::Connection(id));
    AttackPointer attack;
    //@BOTTOM
    if (is_blue || is_attack) {
        //@LEFT
        attack = getAttackK(false, true, is_attack, dx, dy);
        if (validateAttack(id, attack.get()))
            availableConnection.get()->addAttack(attack.get());
        //@RIGHT
        attack = getAttackK(false, false, is_attack, dx, dy);
        if (validateAttack(id, attack.get()))
            availableConnection.get()->addAttack(attack.get());
    }
    //@TOP
    if (!is_blue || is_attack) {
        //@LEFT
        attack = getAttackK(true, true, is_attack, dx, dy);
        if (validateAttack(id, attack.get()))
            availableConnection.get()->addAttack(attack.get());
        //@RIGHT
        attack = getAttackK(true, false, is_attack, dx, dy);
        if (validateAttack(id, attack.get()))
            availableConnection.get()->addAttack(attack.get());
    }
    return availableConnection;
}

AttackPointer Artificial::Replica::getAttackK(bool go_up, bool go_left, bool is_attack, unsigned int dx, unsigned int dy)
{
    //@DECLARE
    bool is_odd = isOdd(dy);
    int mod_up = (go_up) ? -1 : 1;
    int mod_left = (go_left) ? -1 : 1;
    QPointF posx, posy;
    //@TARGET
    if ((is_odd && go_left) || (!is_odd && !go_left))
        posx = QPointF(dx, dy + mod_up);
    else
        posx = QPointF(dx + mod_left, dy + mod_up);
    //@DEST OR HALF
    posy = is_attack ? QPointF(dx + mod_left, dy + mod_up * 2) : posx;
    return std::make_shared<Artificial::Actors::Attack>(posx, posy);
}

bool Artificial::Replica::validateAttack(unsigned int id, Actors::Attack* attack)
{
    //@DECLARE
    QPointF destination = attack->destination;
    QPointF target = attack->target;
    //@RANGE
    if (!inRange(destination.x(), destination.y()) || !inRange(target.x(), target.y()))
        return false;
    //@SIMPLE
    bool is_destination_empty = board[destination.y()][destination.x()] == 0;
    if (destination == target)
        return is_destination_empty ? true : false;
    //@COMPLEX
    bool is_target_empty = board[target.y()][target.x()] == 0;
    if (!is_destination_empty || is_target_empty) return false;
    //@TARGET
    bool is_id_blue = isBlue(id);
    bool is_target_blue = isBlue(board[target.y()][target.x()]);
    if (is_id_blue && !is_target_blue) return true;
    else if (!is_id_blue && is_target_blue) return true;
    else return false;
}

bool Artificial::Replica::crown(unsigned int dx, unsigned int dy)
{
    if (inRange(dx, dy)) {
        int id = board[dy][dx];
        if (id != 0) {
            int index = getConnectionIndex(id, forceVector);
            if (index != -1) {
                ((ConnectionPointer) forceVector.at(index)).get()->crownOwner(THRONE_ID);
            }
            board[dy][dx] += THRONE_ID;
            return true;
        }
    }
    return false;
}

void Artificial::Replica::makeMovement(unsigned int dx, unsigned int dy, Artificial::Actors::Attack attack)
{
    //@DECLARE
    unsigned int dest_x = attack.destination.x(), dest_y = attack.destination.y();
    unsigned int id = board[dy][dx];
    bool is_blue = isBlue(id);
    //@BOARD
    board[dest_y][dest_x] = board[dy][dx];
    board[dy][dx] = 0;
    //@CROWNING
    if (!isThrone(is_blue, id) && isBottom(is_blue, dest_y))
        crown(dest_x, dest_y);
}

void Artificial::Replica::destroyTarget(Artificial::Actors::Attack attack, unsigned int)
{
    board[attack.target.y()][attack.target.x()] = 0;
}

void Artificial::Replica::printBoard()
{
    for (unsigned int i = 0; i < BOARD_HEIGHT; i++) {
        for (unsigned int j = 0; j < BOARD_WIDTH; j++) {
            std::cout << i << ":" << j << ":" << board[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

unsigned int Artificial::Replica::getCurrentTurn()
{
    return current_player_turn;
}

Artificial::Enums::Synchronization Artificial::Replica::getSyncStatus()
{
    return sync_status;
}

void Artificial::Replica::endTurn()
{
    if (current_player_turn == PLAYER_BLUE_ID) {
        current_player_turn = PLAYER_RED_ID;
    }
    else {
        current_player_turn = PLAYER_BLUE_ID;
    }
}

void Artificial::Replica::printForce()
{
    for (unsigned int i = 0, size = forceVector.size(); i < size; i++) {
        ConnectionPointer connection = forceVector.at(i);
        std::vector<Artificial::Actors::Attack> attacks = connection.get()->getAttacks();
        for (unsigned int j = 0, xsize = connection.get()->getAttacks().size(); j < xsize; j++) {
            Artificial::Actors::Attack attack = attacks.at(j);
            std::cout << "Forced: " << connection.get()->getOwner()
                      << " target: " << attack.target.x() << ":" << attack.target.y()
                      << " destination: " << attack.destination.x() << ":" << attack.destination.y() << std::endl;
        }
    }
}

QPointF Artificial::Replica::getPosition(unsigned int id)
{
    QPointF position;
    for (position.ry() = 0; position.ry() < BOARD_HEIGHT; position.ry()++) {
        for (position.rx() = 0; position.rx() < BOARD_WIDTH; position.rx()++){
            if (board[position.ry()][position.rx()] == id) return position;
        }
    }
    return QPointF(-1, -1);
}

bool Artificial::Replica::isBlue(unsigned int id)
{
    return (PLAYER_BLUE_ID - 1) < id;
}

bool Artificial::Replica::isThrone(bool is_blue, unsigned int id)
{
    if (is_blue) id -= PLAYER_BLUE_ID;
    else id -= PLAYER_RED_ID;
    int is_throne = (int) id - (int) THRONE_ID;
    return (is_throne >= 0) ? true : false;
}

bool Artificial::Replica::isBottom(bool is_blue, unsigned int dy)
{
    return is_blue ? (dy == BOARD_HEIGHT - 1) : (dy == 0);
}

bool Artificial::Replica::isOdd(unsigned int dy)
{
    return (dy % 2) == 1;
}

bool Artificial::Replica::isCorner(bool is_odd, unsigned int dx)
{
    return is_odd ? (dx == BOARD_WIDTH - 1) : (dx == 0);
}

int Artificial::Replica::getSolutionIndex(std::vector<int> solutions, bool b_greater)
{
    int index = 0, value = solutions.at(index);
    for (unsigned int i = 1, size = solutions.size(); i < size; i++) {
        if ((b_greater && value < solutions.at(i)) || (!b_greater && value > solutions.at(i))) {
            index = i;
            value = solutions.at(index);
        }
    }
    return index;
}

int Artificial::Replica::getConnectionIndex(unsigned int id, std::vector<ConnectionPointer> connection)
{
    for (int index = 0, size = connection.size(); index < size; index++) {
        if (id == ((ConnectionPointer) connection.at(index)).get()->getOwner()) return index;
    }
    return -1;
}

unsigned int Artificial::Replica::flipTurn(unsigned int turn)
{
    if (turn == PLAYER_BLUE_ID) return PLAYER_RED_ID;
    else return PLAYER_BLUE_ID;
}

bool Artificial::Replica::inRange(int dx, int dy)
{
    return (0 <= dx && dx < ((int) BOARD_WIDTH) && 0 <= dy && dy < ((int) BOARD_HEIGHT)) ? true : false;
}

Artificial::Game::Game(QGraphicsView* view, QMenu* menu) : Replica()
{
    this->view = ViewPointer(view);
    initBoard();
    initScene();
    initTokens();
    this->menu = MenuPointer(menu);
    initTurn();
}

void Artificial::Game::reset()
{
    ScenePointer empty = ScenePointer(new QGraphicsScene());
    view.get()->setScene(empty.get());
//    clearObjects();
//    initBoard();
//    initScene();
//    initTokens();
//    initTurn();
}

QPointF Artificial::Game::boardToScene(QPointF boardPoint)
{
    unsigned int mod_odd = ((int) boardPoint.y()) % 2;
    return QPointF(mod_odd * PIXELS_MARGIN + boardPoint.x() * PIXELS_MARGIN * 2, boardPoint.y()* PIXELS_MARGIN);
}

QPointF Artificial::Game::sceneToBoard(QPointF scenePoint)
{
    unsigned int dy = scenePoint.y() / PIXELS_MARGIN;
    unsigned int mod_odd = dy % 2;
    unsigned int dx = (scenePoint.x() - mod_odd * PIXELS_MARGIN ) / (PIXELS_MARGIN * 2);
    return QPointF(dx, dy);
}

void Artificial::Game::initBoard()
{
    board.clear();
    for (unsigned int i = 0; i < BOARD_HEIGHT; i++) {
        std::vector<unsigned int> board_i;
        for (unsigned int j = 0; j < BOARD_WIDTH; j++) {
            board_i.push_back(0);
        }
        board.push_back(board_i);
    }
    for (unsigned int i = 0; i < TOKENS_PER_PLAYER; i++) {
        //@DECLARE
        unsigned int dy = i / TOKENS_PER_ROW;
        unsigned int dx = i - dy * BOARD_WIDTH;
        unsigned int red_dy = BOARD_HEIGHT - dy - 1;
        //@BOARD
        board[dy][dx] = PLAYER_BLUE_ID + i;
        board[red_dy][dx] = PLAYER_RED_ID + i;
    }
}

void Artificial::Game::initScene()
{
    scene = ScenePointer(new QGraphicsScene());
    QPixmap pixmap(":/sprites/board.png");
    scene.get()->setSceneRect(0, 0, pixmap.width(), pixmap.height());
    scene.get()->addPixmap(pixmap);
    view.get()->setScene(scene.get());
}

void Artificial::Game::initTokens()
{
    QPixmap red_pixmap(":/sprites/token_red.png");
    QPixmap blue_pixmap(":/sprites/token_blue.png");
    for (unsigned int i = 0; i < TOKENS_PER_PLAYER; i++) {
        //@DECLARE
        TokenPointer blue = TokenPointer(new Artificial::Actors::Token(GamePointer(this), PLAYER_BLUE_ID, i, blue_pixmap));
        TokenPointer red = TokenPointer(new Artificial::Actors::Token(GamePointer(this), PLAYER_RED_ID, i, red_pixmap));
        unsigned int dy = i / TOKENS_PER_ROW;
        unsigned int dx = i - dy * BOARD_WIDTH;
        unsigned int red_dy = BOARD_HEIGHT - dy - 1;
        //@BOARD
        blue.get()->setDXDY(dx, dy);
        red.get()->setDXDY(dx, red_dy);
        //@SCENE
        blue.get()->setPos(boardToScene(QPoint(dx, dy)));
        red.get()->setPos(boardToScene(QPointF(dx, red_dy)));
        scene->addItem(red.get());
        scene->addItem(blue.get());
        //@STORE
        red_tokens.push_back(red);
        blue_tokens.push_back(blue);
    }
}

void Artificial::Game::initTurn()
{
    menu->setTitle(TURN_RED);
    current_player_turn = PLAYER_RED_ID;
}

void Artificial::Game::clearObjects()
{
    for (int i = red_tokens.size() - 1; i > 0; i--) {
        ((TokenPointer) red_tokens.at(i)).get()->destroy();
        ((TokenPointer) blue_tokens.at(i)).get()->destroy();
    }
    red_tokens.clear();
    blue_tokens.clear();
    scene.get()->clear();
    scene.reset();
}

void Artificial::Game::playAI()
{
    unsigned int request_turn = current_player_turn;
    while (request_turn == current_player_turn && !endGame) {
        //@BOARD
        std::cout << "Play AI." << std::endl;
        ConnectionPointer connection = search(MINIMAX_DEPTH);
        unsigned int id = connection.get()->getOwner();
        if (id == ERROR_ID) {
            std::cout << "End of game." << std::endl;
            return;
        }
        QPointF position = getPosition(id);
        Artificial::Actors::Attack attack = connection.get()->getAttacks().at(0);
        requestMovement(position.x(), position.y(), attack);
        std::cout << "After request." << std::endl;
        //@SCENE
        if (RED_IS_AI) {
            int index = getTokenIndex(id, red_tokens);
            if (index != -1) {
                //@!!!ANIMATE
                ((TokenPointer) red_tokens.at(index)).get()->setPos(boardToScene(attack.destination));
            }
        }
        else {
            int index = getTokenIndex(id, blue_tokens);
            if (index != -1) {
                //@!!!ANIMATE
                ((TokenPointer) blue_tokens.at(index)).get()->setPos(boardToScene(attack.destination));
            }
        }
    }
}

bool Artificial::Game::crown(unsigned int dx, unsigned int dy)
{
    if (inRange(dx, dy)) {
        unsigned int id = board[dy][dx];
        if (id != 0) {
            //@TOKEN
            int index;
            if (isBlue(id)) {
                index = getTokenIndex(id, blue_tokens);
                if (index != -1) ((TokenPointer) blue_tokens.at(index)).get()->crown(THRONE_ID);
            }
            else {
                index = getTokenIndex(id, red_tokens);
                if (index != -1) ((TokenPointer) red_tokens.at(index)).get()->crown(THRONE_ID);
            }
            //@FORCE
            index = getConnectionIndex(id, forceVector);
            if (index != -1) {
                std::cout << "Forced Crowning at: " << index << std::endl;
                ((ConnectionPointer) forceVector.at(index)).get()->crownOwner(THRONE_ID);
            }
            board[dy][dx] += THRONE_ID;
            return true;
        }
    }
    return false;
}

void Artificial::Game::destroyTarget(Artificial::Actors::Attack attack, unsigned int target_id)
{
    if (isBlue(target_id)) {
        int blue_index = getTokenIndex(target_id, blue_tokens);
        if (blue_index != -1) {
            scene.get()->removeItem(((TokenPointer) blue_tokens.at(blue_index)).get());
        }
    }
    else {
        int red_index = getTokenIndex(target_id, red_tokens);
        if (red_index != -1) {
            scene.get()->removeItem(((TokenPointer) red_tokens.at(red_index)).get());
        }
    }
    board[attack.target.y()][attack.target.x()] = 0;
}

void Artificial::Game::endTurn()
{
    this->scene.get()->update(this->scene.get()->sceneRect());
    if (isEndGame(flipTurn(current_player_turn))) {
        std::cout << "End of game." << std::endl;
        endGame = true;
    }
    else if (current_player_turn == PLAYER_BLUE_ID) {
        menu.get()->setTitle(TURN_RED);
        current_player_turn = PLAYER_RED_ID;
        if (RED_IS_AI) {
            playAI();
        }
    }
    else {
        menu.get()->setTitle(TURN_BLUE);
        current_player_turn = PLAYER_BLUE_ID;
        if (BLUE_IS_AI) {
            playAI();
        }
    }
}

bool Artificial::Game::isEndGame(unsigned int player)
{
    for (unsigned int i = 0; i < BOARD_HEIGHT; i++) {
        for (unsigned int j = 0; j < BOARD_WIDTH; j++) {
            if (board[i][j] != 0) {
                if (player == PLAYER_BLUE_ID) {
                    if (isBlue(board[i][j])) return false;
                }
                else {
                    if (!isBlue(board[i][j])) return false;
                }
            }
        }
    }
    return true;
}

int Artificial::Game::getTokenIndex(unsigned int target_id, std::vector<TokenPointer> tokens)
{
    for (int target_index = 0, size = tokens.size(); target_index < size; target_index++) {
        if (target_id == ((TokenPointer) tokens.at(target_index)).get()->getId()) return target_index;
    }
    return -1;
}
