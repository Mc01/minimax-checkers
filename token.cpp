#include "token.h"

Artificial::Actors::Attack::Attack(QPointF destination)
{
    this->target = destination;
    this->destination = destination;
}

Artificial::Actors::Attack::Attack(QPointF target, QPointF destination)
{
    this->target = target;
    this->destination = destination;
}

Artificial::Actors::Connection::Connection(unsigned int owner)
{
    this->owner = owner;
}

Artificial::Actors::Connection::Connection(unsigned int owner, std::vector<Artificial::Actors::Attack> attacks)
{
    this->owner = owner;
    this->attacks = attacks;
}

void Artificial::Actors::Connection::eatConnection(ConnectionPointer connection)
{
    std::vector<Artificial::Actors::Attack> attacks = connection.get()->getAttacks();
    for (unsigned int i = 0, size = attacks.size(); i < size; i++) {
        AttackPointer attack = std::make_shared<Artificial::Actors::Attack> (attacks.at(i));
        this->addAttack(attack.get());
    }
}

void Artificial::Actors::Connection::addAttack(Artificial::Actors::Attack* attack)
{
    attacks.push_back(*attack);
}

void Artificial::Actors::Connection::crownOwner(unsigned int throne_id)
{
    this->owner += throne_id;
}

unsigned int Artificial::Actors::Connection::getOwner() const
{
    return owner;
}

std::vector<Artificial::Actors::Attack> Artificial::Actors::Connection::getAttacks() const
{
    return attacks;
}

Artificial::Actors::Token::Token(GamePointer game, unsigned int owner_id, unsigned int id, QPixmap& pixmap) : QGraphicsPixmapItem(pixmap)
{
    this->game = game;
    this->owner_id = owner_id;
    this->id = id;
}

void Artificial::Actors::Token::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if ((bValid = isValid())) {
            start_position = this->scenePos();
            drag_position = event->scenePos();
    }
}

void Artificial::Actors::Token::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    if (bValid) {
        qreal dx = event->scenePos().x() - drag_position.x();
        qreal dy = event->scenePos().y() - drag_position.y();
        this->moveBy(dx, dy);
        drag_position = event->scenePos();
    }
}

void Artificial::Actors::Token::mouseReleaseEvent(QGraphicsSceneMouseEvent*)
{
    if (bValid) {
        //@CLOSEST
        std::vector<Attack> attacks = availableMovment.get()->getAttacks();
        Artificial::Actors::Attack closestAttack = attacks.at(0);
        for (unsigned int i = 1, size = attacks.size(); i < size; i++) {
            Artificial::Actors::Attack comparisonAttack = attacks.at(i);
            if (getDistance(this->scenePos(), Artificial::Game::boardToScene(closestAttack.destination)) >
                            getDistance(this->scenePos(), Artificial::Game::boardToScene(comparisonAttack.destination)))
                closestAttack = comparisonAttack;
        }
        //@MATCH
        QPointF closestPoint = Artificial::Game::boardToScene(closestAttack.destination);
        bool b_success = false;
        if (getDistance(this->scenePos(), closestPoint) < MATCH_DISTANCE) {
            if (game.get()->requestMovement(this->dx, this->dy, closestAttack)) {
                b_success = true;
            }
        }
        //@RESULT
        if (b_success) {
            std::cout << "Hit." << std::endl;
            this->setPos(closestPoint);
            this->dx = closestAttack.destination.x();
            this->dy = closestAttack.destination.y();
        }
        else {
            std::cout << "Miss." << std::endl;
            this->setPos(start_position);
        }
    }
}

unsigned int Artificial::Actors::Token::getId() const
{
    return owner_id + id;
}

unsigned int Artificial::Actors::Token::getDy() const
{
    return dy;
}

unsigned int Artificial::Actors::Token::getDx() const
{
    return dx;
}

void Artificial::Actors::Token::setDXDY(unsigned int dx, unsigned int dy)
{
    this->dx = dx;
    this->dy = dy;
}

void Artificial::Actors::Token::crown(unsigned int throne_id)
{
    id += throne_id;
    if (this->owner_id == Artificial::Game::PLAYER_RED_ID)
        this->setPixmap(QPixmap(":/sprites/queen_red.png"));
    else
        this->setPixmap(QPixmap(":/sprites/queen_blue.png"));
}

void Artificial::Actors::Token::destroy()
{
    ScenePointer scene = ScenePointer(this->scene());
    scene.get()->removeItem(this);
    this->~Token();
}

bool Artificial::Actors::Token::isValid()
{
    //@TURN AND AI
    if (owner_id != game.get()->getCurrentTurn()) return false;
    else if (owner_id == Artificial::Game::PLAYER_BLUE_ID) {
        if (Artificial::Game::BLUE_IS_AI) return false;
    }
    else if (Artificial::Game::RED_IS_AI) return false;
    //@AVAILABLE MOVEMENT
    availableMovment = game.get()->getAvailableMovement(this->dx, this->dy);
    if (availableMovment.get()->getAttacks().size() == 0) return false;
    else return true;
}

unsigned int Artificial::Actors::Token::getDistance(QPointF first, QPointF second)
{
    return std::sqrt(std::pow(first.x() - second.x(), 2) + std::pow(first.y() - second.y(), 2));
}

Artificial::Actors::State::State(unsigned int turn, std::vector<std::vector<unsigned int>> board)
{
    this->turn = turn;
    this->board = board;
}

unsigned int Artificial::Actors::State::getTurn() const
{
    return turn;
}

std::vector<std::vector<unsigned int>> Artificial::Actors::State::getBoard() const
{
    return board;
}
