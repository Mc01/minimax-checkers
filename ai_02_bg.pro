#-------------------------------------------------
#
# Project created by QtCreator 2013-11-24T19:47:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ai_02_bg
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    game.cpp \
    token.cpp

HEADERS  += mainwindow.h \
    game.h \
    token.h

FORMS    += mainwindow.ui
CONFIG += c++11

OTHER_FILES += \
    token_red.png \
    token_blue.png \
    board.png \
    token-red.png \
    queen_red.png \
    queen_blue.png

RESOURCES += \
    resources.qrc
