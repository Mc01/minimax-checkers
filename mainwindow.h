#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "game.h"

typedef std::shared_ptr<Artificial::Game> GamePointer;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_red_is_hm_triggered();
    void on_red_is_ai_triggered();
    void on_blue_is_hm_triggered();
    void on_blue_is_ai_triggered();
    void on_new_game_triggered();

    void on_ai_is_4_triggered();

    void on_ai_is_6_triggered();

    void on_ai_is_8_triggered();

private:
    Ui::MainWindow *ui;
    GamePointer game;
    void syncPlayers();
};

#endif // MAINWINDOW_H
