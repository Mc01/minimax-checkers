#ifndef GAME_H
#define GAME_H

namespace Artificial {
    class Game;
    class Replica;
}

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QMenu>
#include <memory>
#include "token.h"

typedef std::shared_ptr<QGraphicsView> ViewPointer;
typedef std::shared_ptr<QGraphicsScene> ScenePointer;
typedef std::shared_ptr<QMenu> MenuPointer;
typedef std::shared_ptr<Artificial::Actors::Token> TokenPointer;
typedef std::shared_ptr<Artificial::Actors::Connection> ConnectionPointer;
typedef std::shared_ptr<Artificial::Actors::Attack> AttackPointer;
typedef std::shared_ptr<Artificial::Actors::State> StatePointer;
typedef std::shared_ptr<Artificial::Replica> ReplicaPointer;

namespace Artificial {
    namespace Enums {
        enum Synchronization {
            NONE,
            SIMPLE,
            COMPLEX
        };
    }

    class Replica
    {
    public:
        Replica();
        Replica(StatePointer);
        //@CONST
        static const unsigned int PLAYER_RED_ID = 100;
        static const unsigned int PLAYER_BLUE_ID = 200;
        static const unsigned int ERROR_ID = 300;
        static const unsigned int THRONE_ID = 50;
        //@PARAM
        static unsigned int MINIMAX_DEPTH;
        //@METHOD
        int miniMax(int, bool);
        ConnectionPointer search(int);
        bool synchronize(ConnectionPointer);
        int getEvaluation();
        ConnectionPointer getAvailableMovement(unsigned int, unsigned int);
        bool requestMovement(unsigned int, unsigned int, Artificial::Actors::Attack);
        void printBoard();
        //@ACCESSORS
        unsigned int getCurrentTurn();
        Artificial::Enums::Synchronization getSyncStatus();
    protected:
        //@CONST
        static const unsigned int BOARD_HEIGHT = 8;
        static const unsigned int BOARD_WIDTH = 4;
        //@VAR
        unsigned int current_player_turn;
        Artificial::Enums::Synchronization sync_status = Artificial::Enums::NONE;
        std::vector<std::vector<unsigned int>> board;
        std::vector<ConnectionPointer> forceVector;
        std::vector<ReplicaPointer> cloneVector;
        //@METHOD
        std::vector<ConnectionPointer> gather();
        std::vector<ReplicaPointer> replicate(std::vector<ConnectionPointer>);
        bool isolate(ConnectionPointer connection);
        virtual bool crown(unsigned int, unsigned int);
        void makeMovement(unsigned int, unsigned int, Artificial::Actors::Attack);
        virtual void destroyTarget(Artificial::Actors::Attack, unsigned int);
        virtual void endTurn();
        bool renewForce(unsigned int);
        ConnectionPointer getConnectionK(bool, unsigned int, unsigned int);
        ConnectionPointer getQueenK(bool, bool, bool, unsigned int, unsigned int);
        ConnectionPointer getPathK(bool, bool, unsigned int, unsigned int);
        AttackPointer getAttackK(bool, bool, bool, unsigned int, unsigned int);
        bool validateAttack(unsigned int, Artificial::Actors::Attack*);
        void printForce();
        QPointF getPosition(unsigned int);
        //@STATIC
        static bool isBlue(unsigned int);
        static bool isThrone(bool, unsigned int);
        static bool isBottom(bool, unsigned int);
        static bool isOdd(unsigned int);
        static bool isCorner(bool, unsigned int);
        static int getSolutionIndex(std::vector<int>, bool);
        static int getConnectionIndex(unsigned int, std::vector<ConnectionPointer>);
        static unsigned int flipTurn(unsigned int);
        static bool inRange(int, int);
    };

    class Game : public Replica
    {
    public:
        Game(QGraphicsView*, QMenu*);
        //@CONST
        static const unsigned int TOKENS_PER_PLAYER = 12;
        static const unsigned int TOKENS_PER_ROW = 4;
        //@PARAMS
        static bool RED_IS_AI;
        static bool BLUE_IS_AI;
        //@METHOD
        void reset();
        void playAI();
        //@STATIC
        static QPointF boardToScene(QPointF);
        static QPointF sceneToBoard(QPointF);
    private:
        //@CONST
        static const unsigned int PIXELS_MARGIN = 75;
        static const QString TURN_RED;
        static const QString TURN_BLUE;
        //@VAR
        ViewPointer view;
        ScenePointer scene;
        std::vector<TokenPointer> red_tokens;
        std::vector<TokenPointer> blue_tokens;
        bool endGame = false;
        //@PARAMS
        static MenuPointer menu;
        //@METHOD
        void initBoard();
        void initScene();
        void initTokens();
        void initTurn();
        void clearObjects();
        bool crown(unsigned int, unsigned int);
        void destroyTarget(Artificial::Actors::Attack, unsigned int);
        void endTurn();
        bool isEndGame(unsigned int);
        //@STATIC
        static int getTokenIndex(unsigned int, std::vector<TokenPointer>);
    };
}

#endif // GAME_H
